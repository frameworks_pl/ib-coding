<?php

namespace App\Domain\Models;

use App\Domain\Enums\StatusEnum;
use Illuminate\Support\Facades\App;
use App\Modules\Approval\Api\ApprovalFacadeInterface;
use App\Modules\Approval\Api\Dto\ApprovalDto;
use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Uuid4;
use Ramsey\Uuid\UuidInterface;

/**
 * @property string $id
 * @property string $company_id
 * @property string $number
 * @property string $date
 * @property string $due_date
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 * @property Company $company
 * @property InvoiceProductLine[] $invoiceProductLines
 */
class Invoice extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['company_id', 'number', 'date', 'due_date', 'status', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo('App\Domain\Models\Company');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function invoiceProductLines()
    {
        return $this->hasMany('App\Domain\Models\InvoiceProductLine');
    }

    public function getId() : UuidInterface {
        $uuid = Uuid::getFactory()->fromString($this->attributes['id']);
        return $uuid;
    }

    public function setId(UuidInterface $uuid) {
        $this->attributes['id'] = $uuid->toString();
    }

    public function getStatus() : StatusEnum {
        return StatusEnum::from($this->attributes['status']);
    }

    public function setStatus(StatusEnum $status) {
        $this->attributes['status'] = $status->value;
    }

}
