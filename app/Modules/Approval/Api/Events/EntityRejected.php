<?php

declare(strict_types=1);

namespace App\Modules\Approval\Api\Events;

use App\Modules\Approval\Api\Dto\ApprovalDto;
use Illuminate\Foundation\Bus\Dispatchable;

final readonly class EntityRejected
{
    use Dispatchable;

    public function __construct(
        public ApprovalDto $approvalDto
    ) {
    }
}
