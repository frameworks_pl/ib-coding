<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Controller;
use App\Modules\Invoices\Dto\InvoiceDto;
use App\Modules\Invoices\Infrastructure\Repositories\InvoiceRepositoryInterface;
use App\Modules\Invoices\Infrastructure\Services\InvoiceServiceInterface;
use Illuminate\Routing\Controller;
use Illuminate\View\View;
use App\Domain\Enums\StatusEnum;
use App\Domain\Model\Invoice;


class InvoiceController extends Controller {

    private $invoicesRepository;
    private $invoiceService;

    public function __construct(InvoiceRepositoryInterface $invoiceRepository,
        InvoiceServiceInterface $invoiceService
    ) {
        $this->invoicesRepository = $invoiceRepository;
        $this->invoiceService = $invoiceService;
    }

    public function show($id): View {
        $invoice = $this->invoicesRepository->getById($id);
        return view('invoice', compact('invoice'));
    }

    public function showAll() : View {
        $invoices = $this->invoicesRepository->getAll();
        return view('invoices', compact('invoices'));
    }

    //This would normally by some API to be called from front-end but for the purpose of this demo will leave it here
    public function save($id) : View
    {
        //For simplicity, I will assume that we are saving invoices that are already in database with status 'DRAFT'
        //if we hit invoice approved/rejected saving will be interrupted by validation (exception thrown from InvoiceService::validate)

        $invoice = $this->invoicesRepository->getById($id);
        $result = null;

        try {
            $this->invoiceService->validate(new InvoiceDto($invoice->getid(), $invoice->getStatus()));
            $invoice->refresh();
        } catch (\Exception $e) {
            $result = $e;
        }

        return view('invoice', compact('invoice', 'result'));

    }
}
