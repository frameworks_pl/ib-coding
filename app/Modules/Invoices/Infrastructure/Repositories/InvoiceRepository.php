<?php
declare(strict_types=1);

namespace App\Modules\Invoices\Infrastructure\Repositories;

use App\Domain\Enums\StatusEnum;
use App\Domain\Models\Invoice;
use App\Infrastructure\Providers\EventServiceProvider;
use App\Modules\Approval\Api\Dto\ApprovalDto;
use App\Modules\Approval\Api\Events\EntityRejected;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Database\Eloquent\Collection;
use TYPO3\CMS\Reports\Status;

class InvoiceRepository implements InvoiceRepositoryInterface
{

    public function __construct(Dispatcher $dispatcher) {

    }

    public function getAll() : Collection
    {
        //TODO: change this to use InvoiceInterface rather than row model collection
        return Invoice::all();
    }

    public function getById($id) : Invoice {
        return Invoice::find($id);
    }

}
