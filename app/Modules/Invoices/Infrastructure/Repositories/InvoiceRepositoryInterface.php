<?php
declare(strict_types=1);

namespace App\Modules\Invoices\Infrastructure\Repositories;

use App\Domain\Models\Invoice;
use Illuminate\Database\Eloquent\Collection;

interface InvoiceRepositoryInterface
{
    public function getAll(): Collection;

    public function getById($id) : Invoice;

}
