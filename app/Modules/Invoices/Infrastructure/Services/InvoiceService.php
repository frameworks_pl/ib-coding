<?php
declare(strict_types=1);

namespace App\Modules\Invoices\Infrastructure\Services;

use App\Modules\Approval\Api\ApprovalFacadeInterface;
use App\Modules\Approval\Api\Dto\ApprovalDto;
use App\Modules\Invoices\Dto\InvoiceDto;
use App\Domain\Enums\StatusEnum;

class InvoiceService implements  InvoiceServiceInterface {

    private ApprovalFacadeInterface $approvalService;

    public function __construct(ApprovalFacadeInterface $approvalService) {
        $this->approvalService = $approvalService;
    }

    public function validate(InvoiceDto $invoiceDto) {

        //TODO: Here real approval/rejection logic would need to be implemented
        //for now I will just randomly approve/deny the invoice

        $approve = (bool)random_int(0, 1);
        if ($approve) {
            $this->approvalService->approve(new ApprovalDto($invoiceDto->id, $invoiceDto->status, "Invoice")); //Not sure what third paramter is for
        } else {
            $this->approvalService->reject(new ApprovalDto($invoiceDto->id, $invoiceDto->status, "Invoice"));
        }



    }

}
