<?php
declare(strict_types=1);

namespace App\Modules\Invoices\Infrastructure\Services;

use App\Modules\Invoices\Dto\InvoiceDto;

interface InvoiceServiceInterface {

    public function validate(InvoiceDto $invoiceDto);

}
