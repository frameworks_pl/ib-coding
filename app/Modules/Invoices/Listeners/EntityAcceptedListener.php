<?php

namespace App\Modules\Invoices\Listeners;

use App\Domain\Enums\StatusEnum;
use App\Modules\Approval\Api\Events\EntityRejected;
use App\Modules\Invoices\Infrastructure\Repositories\InvoiceRepository;

class EntityAcceptedListener
{
    protected InvoiceRepository $invoiceRepository;

    public function __construct(InvoiceRepository $invoiceRepository) {
        $this->invoiceRepository = $invoiceRepository;
    }

    public function handle(EntityRejected $event)
    {
        $invoice = $this->invoiceRepository->getById($event->approvalDto->id);
        $invoice->status = StatusEnum::APPROVED;
        $invoice->save();
    }
}
