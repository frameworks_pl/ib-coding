<div style="margin-bottom:20px">
    @isset($result)
        @if($result != null)
                <p style="color:red">{{ $result->getMessage() }}</p>
        @else
            <p style="color:green">Saving succeeded</p>
        @endif
    @endisset
</div>

<h1>Company</h1>

<table border="1">
    <tr><td>Name</td><td>{{ $invoice->company->name }}</td></tr>
    <tr><td>Street</td><td>{{ $invoice->company->street }}</td></tr>
    <tr><td>City</td><td>{{ $invoice->company->city }}</td></tr>
    <tr><td>Zip</td><td>{{ $invoice->company->zip }}</td></tr>
    <tr><td>Phone</td><td>{{ $invoice->company->phone }}</td></tr>
    <tr><td>Email</td><td>{{ $invoice->company->email }}</td></tr>
    <tr><td>Created</td><td>{{ $invoice->company->created_at }}</td></tr>
    <tr><td>Updated</td><td>{{ $invoice->company->updated_at }}</td></tr>
</table>


<h1>Invoice</h1>
<table border="1">

    <tbody>

        <tr>
            <td>ID:</td>
            <td>{{ $invoice->id }}</td>
        </tr>
        <tr>
            <td>Number</td>
            <td>{{ $invoice->number }}</td>
        </tr>
        <tr>
            <td>Date:</td>
            <td>{{ $invoice->date }}</td>
        </tr>
        <tr>
            <td>Due date:</td>
            <td>{{ $invoice->due_date }}</td>
        </tr>
        <tr>
            <td>Company Id:</td>
            <td>{{ $invoice->company_id }}</td>
        </tr>
        <tr>
            <td>Status:</td>
            <td>{{ $invoice->status }}</td>
        </tr>
        <tr>
            <td>Created:</td>
            <td>{{ $invoice->created_at }}</td>
        </tr>
        <tr>
            <td>Updated:</td>
            <td>{{ $invoice->updated_at }}</td>
        </tr>

    </tbody>
</table>
<table>

<h1>Products</h1>
<table border="1">
<tr>
    <td>Name</td>
    <td>Price</td>
    <td>Currency</td>
    <td>Created</td>
    <td>Updated</td>
</tr>
@foreach ($invoice->invoiceProductLines as $invoiceProductLine)
    <tr>
        <td>{{ $invoiceProductLine->product->name }}</td>
        <td>{{ $invoiceProductLine->product->price }}</td>
        <td>{{ $invoiceProductLine->product->currency }}</td>
        <td>{{ $invoiceProductLine->product->created_at }}</td>
        <td>{{ $invoiceProductLine->product->updated_at }}</td>
    </tr>
@endforeach
</table>

<div style="margin-top:20px">
    <a href="{{ route('invoice_save', ['id' => $invoice->id]) }}">Save</a>
</div>



</table>
