<?php
declare(strict_types=1);

namespace Tests;

use App\Modules\Approval\Api\Dto\ApprovalDto;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use App\Domain\Models\Invoice;

class InvoiceModelTest extends BaseTestCase
{

    use CreatesApplication;

    public function testApprovalDTOFromInvoiceHappyPath() {

        $invoice = new Invoice();
        $invoice->id = "17f568c7-dc18-494a-bca7-982778025128";
        $invoice->status = "approved";

        //If invoice constructor fails to convert strings to value object this test will fail
        $approvalDTO = new ApprovalDto($invoice->getId(), $invoice->getStatus(), "Invoice");
        $this->assertEquals("17f568c7-dc18-494a-bca7-982778025128", $approvalDTO->id->toString());
        $this->assertEquals("approved", $approvalDTO->status->value);

    }

}
